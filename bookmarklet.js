/**
 * redirect javascript bookmarklet
 */
// javascript:location.href='http://linkedmovie.hp.af.cm/#?uri='+encodeURIComponent(location.href)

/**
 * bookmarklet loaded on site
 */
(function(){

  // Load the script from url and when it's ready loading run the callback.
  function loadScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;

    // Attach handlers for all browsers
    var done = false;
    script.onload = script.onreadystatechange = function() {
      if(
          !done && (
          !this.readyState ||
          this.readyState === 'loaded' ||
          this.readyState === 'complete')
        ) {
        done = true;

        // Continue your code
        callback();

        // Handle memory leak in IE
        script.onload = script.onreadystatechange = null;
        document.head.removeChild(script);
      }
    };

    document.head.appendChild(script);
  }

  // Load a list of scripts *one after the other* and run cb
  var loadScripts = function(scripts, cb) {
    var script;
    if(scripts.length) {
      script = scripts.shift();
      loadScript(script, function(){
        loadScripts(scripts.slice(0), cb);
      });
    } else {
      if (cb) { cb(); }
    }
  };

  var loadStyles = function(csss) {
    var css, _i, _len;
    for (_i = 0, _len = csss.length; _i < _len; _i++) {
      css = csss[_i];
      var e = document.createElement('link');
      e.setAttribute('rel','stylesheet');
      e.setAttribute('href', css);
      document.head.appendChild(e);
    }
  };

  var appRoot = 'http://linkedmovie.hp.af.cm/';
  var body = document.body;
  var url = document.URL;
  body.id = 'bookmarklet';
  body.setAttribute('ng-controller', 'LDController');
  body.innerHTML =
    '<header>' +
      '<div class="close">' +
        '<a href="javascript:location.reload()">&times;</a>' +
      '</div>' +
      '<div class="info ng-cloak">' +
        '<span ng-show="urlChanged">{{startURI}} </span>' +
        '<span ng-show="prevResource"><a ng-click="changeResource(prevResource)"" href="">back</a> </span>' +
        '<span ng-hide="warning || urierror || viewsData.length" class="loading loading-spinner"></span>' +
        '<span ng-show="warning">not possible to get any data :(</span>' +
      '</div>' +
      '<div id="views" class="ng-cloak">' +
        '<a ng-click="openView(view.id)" ng-repeat="view in viewsData" href="" ng-class="{active: view.id == selectedView}">' +
          '<span>{{view.id}}</span>' +
        '</a>' +
        '<a ng-click="showIframe()" href="" class="active">' +
          '<span>normal</span>' +
        '</a>' +
      '</div>' +
    '</header>' +
    '<div id="wrapper">' +
      '<div ng-repeat="view in viewsData" id="{{view.id}}" class="view"></div>' +
      '<iframe ng-src="{{startURI}}"></iframe>' +
    '</div>';

   // Loading style definitions
  loadStyles([
    appRoot + 'css/bootstrap.min.css',
    appRoot + 'css/application.css'
  ]);

  // Loading the scripts
  loadScripts([
    'http://code.jquery.com/jquery-2.0.0.min.js',
    'https://ajax.googleapis.com/ajax/libs/angularjs/1.1.4/angular.min.js',
    'https://raw.github.com/digitalbazaar/jsonld.js/master/js/jsonld.js',
    appRoot + 'js/get_resource.js',
    appRoot + 'js/init.js',
    appRoot + 'js/application.js'
  ]);

})();

