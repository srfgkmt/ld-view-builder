var visuaLOD = angular.module('visuaLOD', []);

visuaLOD.service('ld', function($http, $q, $rootScope) {

  var self = this;

  this.getResource = function(uri, context) {
    if(!angular.isString(uri)) {
      var deferred = $q.defer();
      deferred.reject();
      return deferred.promise;
    }
    return self.getJSONLD(uri).then(function(jsonld) {
      return self.compactJSONLD(jsonld, context);
    });
  };

  /**
   * Converts Resource to JSON-LD via rdf-translator and
   * expands it because rdf-translator defines own context
   * @see http://rdf-translator.appspot.com
   * @param  {String} uri
   * @return {Promise}
   */
  this.getJSONLD = function(uri) {
    var deferred = $q.defer(),
        error = 'rdf-translator throws an error with uri: ' + uri;
    $http.get('http://rdf-translator2.appspot.com/convert/detect/json-ld/'+uri).success(function(data) {
      var graph = data['@graph'],
          resource = null;

      angular.forEach(graph, function(r) {
        if(r['@id'] === uri) { // matches uri
          resource = r;
        }
      });
      deferred.resolve(resource || graph)

      // var context = data['@context'];

      // if(angular.isUndefined(context)) {
      //   deferred.reject(error);
      //   return;
      // }

      // jsonld.expand(data, function(err, expanded) { // use expand for later uri comparison
      //   var resource = null;
      //   angular.forEach(expanded, function(r) {

      //     if(r['@id'] === uri) { // matches uri
      //       resource = r;
      //       angular.extend(resource, {'@context': context});
      //     }
      //   });
      //   $rootScope.$apply(deferred.resolve(resource || expanded));
      // });

    }).error(function() {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  /**
   * Uses the the JSON-LD Processor and API Implemenation of jsonld.js
   * to compact JSON-LD with the given context
   * @see https://github.com/digitalbazaar/jsonld.js
   * @param  {Object} input   JSON-LD input
   * @param  {Object} context JSON-LD context
   * @return {Promise}
   */
  this.compactJSONLD = function(input, context) {
    var deferred = $q.defer();

    jsonld.compact(input, context, function(err, compacted) {
      if(!err) {
        $rootScope.$apply(deferred.resolve(compacted));
      } else {
        deferred.reject('JSON-LD compacting');
      }
    });

    return deferred.promise;
  };
});
