/**
 * only for bookmarklet
 */
visuaLOD.run(function ($rootScope) {
  $rootScope.startURI = location.href;
  $rootScope.appRoot = 'http://linkedmovie.hp.af.cm/';

  $rootScope.viewsPath = $rootScope.appRoot + "views/";
  $rootScope.views = ["actor", "person", "movie", "movie_lmdb", "place", "default"];

  $('#wrapper .view').hide();
  $rootScope.selectedView = undefined;

  $rootScope.openView = function(view) {
    $('header #views a').removeClass('active');
    $('#wrapper .view').hide();
    $('#wrapper iframe').hide();
    $('#wrapper #'+view).show();
    $rootScope.selectedView = view;
  };

  $rootScope.showIframe = function() {
    $rootScope.selectedView = undefined;
    $('header #views a').removeClass('active');
    $('#wrapper .view').hide();
    $('#wrapper iframe').show();
    $('header #views a').last().addClass('active');
  };
});
